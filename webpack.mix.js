const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'public/vendors/bootstrap/dist/css/bootstrap.min.css',
    'public/vendors/morris.js/morris.css',
    'public/vendors/jquery-toggles/css/toggles.css',
    'public/vendors/jquery-toggles/css/themes/toggles-light.css',
    'public/vendors/jquery-toast-plugin/dist/jquery.toast.min.css',
    'public/dist/css/style.css',
], 'public/css/all.css');


mix.browserSync({
    proxy: 'localhost:8000'
});
