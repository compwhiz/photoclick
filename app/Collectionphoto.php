<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collectionphoto extends Model
{
    protected $fillable = ['path'];

    protected $appends = [
        'good'
    ];

    public function collection()
    {
        return $this->belongsTo(Collection::class, 'id', 'collection_id');
    }

    public function getGoodAttribute()
    {
        return asset('/app/'.$this->attributes['path']);
    }
}
