<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Collectionphoto;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class CollectionphotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Collectionphoto $photo)
    {
        $path = $request->file('file')->store('collectionPhotos', 's3');
//        $img->save(storage_path('app/images/watermark-test.jpg'));
        $collection = Collection::all()->find($request->collection);

        $photos = new Collectionphoto([
            'path' => $path
        ]);
        $paths = $this->uploadWatermark($request,$path);

        $collection->photos()->save($photos);

        return response()->json($collection);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Collectionphoto $collectionphoto
     * @return \Illuminate\Http\Response
     */
    public function show(Collectionphoto $collectionphoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Collectionphoto $collectionphoto
     * @return \Illuminate\Http\Response
     */
    public function edit(Collectionphoto $collectionphoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Collectionphoto $collectionphoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collectionphoto $collectionphoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Collectionphoto $collectionphoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collectionphoto $collectionphoto)
    {
        //
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function uploadWatermark(Request $request,$path): bool
    {
        $image = new ImageManager();
        $watermark = $image->make(public_path('logoblack.png'));
        $img = $image->make($request->file('file'));
//#1
        $watermarkSize = $img->width() - 20; //size of the image minus 20 margins
//#2
        $watermarkSize = $img->width() / 2; //half of the image size
//#3
        $resizePercentage = 70;//70% less then an actual image (play with this value)
        $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

// resize watermark width keep height auto
        $watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });
//insert resized watermark to image center aligned
        $img->insert($watermark, 'center');
//save new image
        $resource = $img->stream()->detach();

        $imageName = $request->file('file')->getClientOriginalName();
        $path = Storage::disk('s3')->put(
            'watermarked/' . $path,
            $resource
        );
        return $path;
    }
}
