<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Collection extends Model
{

    protected $dates = [
        'date'
    ];
    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope('user', function (Builder $builder) {
//            $builder->where('user_id', \Auth::user()->id);
//        });
    }
        public function photos()
    {
        return $this->hasMany(Collectionphoto::class,'collection_id','id');
    }


}
