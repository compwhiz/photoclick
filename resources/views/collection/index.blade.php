@extends('layouts.backend')

@section('content')
    <!-- Container -->
    <div class="container mt-xl-50 mt-sm-30 mt-15">
        <!-- Title -->
        <div class="hk-pg-header">
            <div>
                <h2 class="hk-pg-title font-weight-600">Collections</h2>
            </div>

            <div class="d-flex mb-0 flex-wrap">
                <button type="button"
                        class="btn btn-sm btn-outline-primary btn-rounded btn-wth-icon icon-wthot-bg mb-15"
                        data-toggle="modal" data-target="#exampleModal">
                    <span class="icon-label">
                        <span class="feather-icon">
                            <i data-feather="plus"></i>
                        </span>
                    </span>
                    <span class="btn-text">new collection</span>
                </button>
            </div>
        </div>
        <!-- /Title -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Collection</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <create></create>
                    </div>
                    {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="hk-row">
            <div class="col-lg-3 col-sm-6">
                <div class="card card-sm">
                    <div class="card-body">
                        <span
                            class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10">Total Collections</span>
                        <div class="d-flex align-items-center justify-content-between position-relative">
                            <div>
                                <span
                                    class="d-block display-5 font-weight-400 text-dark">{{ auth()->user()->collection()->count() }}</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card card-sm">
                    <div class="card-body">
                        <span
                            class="d-block font-11 font-weight-500 text-dark text-uppercase mb-10">Income</span>
                        <div class="d-flex align-items-center justify-content-between position-relative">
                            <div>
												<span class="d-block">
													<span class="display-5 font-weight-400 text-dark"><small>Ksh. </small><span
                                                            class="counter-anim">0.00</span></span>
												</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <index :collections="{{ $collections }}"></index>
    </div>
@endsection
