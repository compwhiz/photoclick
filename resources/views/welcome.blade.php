<!doctype html>
<!-- Mirrored from demo.epic-webdesign.com/tf-essentials/v1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Feb 2019 14:10:23 GMT -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PichaClick</title>
    <meta name="theme-color" content="#017df7"/>
    <meta name="msapplication-navbutton-color" content="#017df7">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#017df7">
    <!-- Loading Bootstrap -->
    <link href="{{ asset('pub/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Loading Template CSS -->
    <link href="{{ asset('pub/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('pub/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('pub/css/style-magnific-popup.css') }}" rel="stylesheet">

    <!-- Awsome Fonts -->
    <link rel="stylesheet" href="{{ asset('pub/css/all.min.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Dosis:500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,600,700" rel="stylesheet">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@PichaClick">
    <meta name="twitter:creator" content="@PichaClick">
    <meta name="twitter:title" content="Made for Photographers by photographers">
    <meta name="twitter:description" content="PichaClick was created to be the fastest and most customizable way for professional photographers to deliver, share, and sell their work.">
    <meta name="twitter:image" content="https://pichaclick.co.ke/RUNCLUB.png">
    <!-- Font Favicon -->
    <link rel="shortcut icon" href="{{ asset('pub/images/favicon.ico') }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-54081021-8"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-54081021-8');
    </script>

</head>
<body>

<!--begin header -->
<header class="header">

    <!--begin navbar-fixed-top -->
    <nav class="navbar navbar-default navbar-fixed-top">

        <!--begin container -->
        <div class="container">

            <!--begin navbar -->
            <nav class="navbar navbar-expand-lg">

                <!--begin logo -->
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('front.png') }}" alt="PichaClick">
                </a>
                <!--end logo -->

                <!--begin navbar-toggler -->
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                </button>
                <!--end navbar-toggler -->

                <!--begin navbar-collapse -->
                <div class="navbar-collapse collapse" id="navbarCollapse" style="">

                    <!--begin navbar-nav -->
                    <ul class="navbar-nav ml-auto">

                        <li><a href="#home">Features</a></li>

                        <li><a href="#about">Examples</a></li>

                        <li><a href="#pricing">Pricing</a></li>

                        <li><a href="{{ route('login') }}" class="external">Login</a></li>

                        <li class="discover-link"><a href="/register" class="external discover-btn">Get Started</a></li>

                    </ul>
                    <!--end navbar-nav -->

                </div>
                <!--end navbar-collapse -->

            </nav>
            <!--end navbar -->

        </div>
        <!--end container -->

    </nav>
    <!--end navbar-fixed-top -->

</header>
<!--end header -->

<div id="front">
    <!--begin home section -->
    <section class="home-section" id="home">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-6-->
                <div class="col-md-6 padding-top-80">

                    <h1>Galleries Designed to Help You Succeed</h1>

                    <p>Made by photographers to simplify your online sales, proofing, and delivery.</p>

                    <a href="/register" class="btn-blue scrool">
                        Get Started</a>

                </div>
                <!--end col-md-6-->

                <!--begin col-md-6-->
                <div class="col-md-6">

                    <img src="{{ asset('storeowner.png') }}" class="hero-image width-100" alt="pic">

                </div>
                <!--end col-md-6-->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </section>
    <!--end home section -->

    <!--begin section-grey -->
    <section class="section-grey section-top-border" id="about">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">

                    <h2 class="section-title">Impress Your Clients and Set Yourself Apart</h2>

                    <p class="section-subtitle">Beautiful covers and clean gallery layouts tailored to fit you.</p>

                </div>
                <!--end col-md-12 -->

                <!--begin col-md-4 -->
                <div class="col-md-3">

                    <div class="main-services">

                        <img src="{{ asset('portfolio.png') }}" class="width-100" alt="pic">

                        <h3>Portfolio</h3>

                        {{--<p>Curabitur quam etsum lacus netsum nulat iaculis ets vitae etsum nisle varius sed aliquam ets--}}
                        {{--vitae netsum.</p>--}}

                        {{--<a href="#about" class="btn-blue-line small scrool">Explore--}}

                        {{--<i class="fa fa-forward"></i></a>--}}

                    </div>

                </div>
                <!--end col-md-3 -->

                <!--begin col-md-3 -->
                <div class="col-md-3">

                    <div class="main-services">

                        <img src="{{ asset('curriculum.png') }}" class="width-100" alt="pic">

                        <h3>Covers</h3>

                        {{--<p>Curabitur quam etsum lacus netsum nulat iaculis ets vitae etsum nisle varius sed aliquam ets--}}
                        {{--vitae netsum.</p>--}}

                        {{--<a href="#about" class="btn-blue-line small scrool">Explore <i class="fa fa-forward"></i></a>--}}

                    </div>

                </div>
                <!--end col-md-3 -->

                <!--begin col-md-3 -->
                <div class="col-md-3">

                    <div class="main-services">

                        <img src="{{ asset('gallery.png') }}" class="width-100" alt="pic">

                        <h3>Galleries</h3>

                        {{--<p>Curabitur quam etsum lacus netsum nulat iaculis ets vitae etsum nisle varius sed aliquam ets--}}
                        {{--vitae netsum.</p>--}}

                        {{--<a href="#about" class="btn-blue-line small scrool">Explore <i class="fa fa-forward"></i></a>--}}

                    </div>

                </div>
                <div class="col-md-3">

                    <div class="main-services">

                        <img src="{{ asset('php-code.png') }}" class="width-100" alt="pic">

                        <h3>Embed</h3>

                        {{--<p>Curabitur quam etsum lacus netsum nulat iaculis ets vitae etsum nisle varius sed aliquam ets--}}
                        {{--vitae netsum.</p>--}}

                        {{--<a href="#about" class="btn-blue-line small scrool">Explore <i class="fa fa-forward"></i></a>--}}

                    </div>

                </div>
                <!--end col-md-4 -->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </section>
    <!--end section-grey -->

    <!--begin section-white -->
    <section class="section-white section-top-border">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-6 -->
                <div class="col-md-6 padding-top-30">

                    <!--begin features-second -->
                    <div class="features-second">

                        <div class="dropcaps-circle">
                            <i class="fa fa-envelope"></i>
                        </div>

                        <h4 class="margin-bottom-5">Get an Email </h4>

                        <p>When all photos are ready, you will receive an email.</p>

                    </div>
                    <!--end features-second-->

                    <!--begin features-second-->
                    <div class="features-second">

                        <div class="dropcaps-circle">
                            <i class="fa fa-business-time"></i>
                        </div>

                        <h4 class="margin-bottom-5">Click a Download button</h4>

                        <p>Click on the link on the email to start the download process on your smartphone or
                            computer.</p>

                    </div>
                    <!--end features-second-->

                    <!--begin features-second-->
                    <div class="features-second">

                        <div class="dropcaps-circle">
                            <i class="fa fa-download"></i>
                        </div>

                        <h4 class="margin-bottom-5">Download Photos</h4>

                        <p>Photos are downloaded to your computer or smartphone</p>

                    </div>
                    <!--end features-second-->

                </div>
                <!--end col-md-6-->

                <!--begin col-md-6-->
                <div class="col-md-6 wow slideInRight" data-wow-delay="0.25s"
                     style="visibility: visible; animation-delay: 0.25s; animation-name: slideInRight;">

                    <img src="{{ asset('illustrated.png') }}" class="width-100 box-shadow" alt="pic">

                </div>
                <!--end col-md-6-->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </section>
    <!--end section-white -->
    <!--begin services section -->
    <section class="section-white section-bottom-border" id="features">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-12-->
                <div class="col-md-12 text-center padding-bottom-10">

                    <h2 class="section-title">Earn More with Online Sales</h2>

                    <p class="section-subtitle">Dedicated online photo gallery for each of your clients, with beautiful
                        cover and layout right out of the box.</p>

                </div>
                <!--end col-md-12 -->

            </div>
            <!--end row -->

            <!--begin row -->
            <div class="row">

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="feature-box wow fadeIn" data-wow-delay="0.25s"
                         style="visibility: visible; animation-delay: 0.25s; animation-name: fadeIn;">

                        <i class="fas fa-tachometer-alt"></i>

                        <h4>Set your price</h4>

                        <p>PichaClick supports over 130 currencies</p>

                    </div>
                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="feature-box wow fadeIn" data-wow-delay="0.5s"
                         style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">

                        <i class="fas fa-chart-line"></i>

                        <h4>0% Commission</h4>

                        <p>Keep all of your profits on every sale</p>

                    </div>
                </div>
                <!--end col-md-4 -->

                <!--begin col-md 4-->
                <div class="col-md-4">

                    <div class="feature-box wow fadeIn" data-wow-delay="0.75s"
                         style="visibility: visible; animation-delay: 0.75s; animation-name: fadeIn;">

                        <i class="fas fa-bell"></i>

                        <h4>Digital sales</h4>

                        <p>Full control to sell any single image or the entire gallery</p>

                    </div>
                </div>
                <!--end col-md-4 -->

            </div>
            <!--end row -->

            <!--begin row -->
            <div class="row">

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="feature-box wow fadeIn" data-wow-delay="1s"
                         style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;">

                        <i class="fas fa-hand-holding-usd"></i>

                        <h4>Discount codes</h4>

                        <p>Boost sales with with custom discount codes</p>

                    </div>
                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="feature-box wow fadeIn" data-wow-delay="1.25s"
                         style="visibility: visible; animation-delay: 1.25s; animation-name: fadeIn;">

                        <i class="fas fa-rocket"></i>

                        <h4>Gallery Banners</h4>

                        <p>Promote a sale or add a custom message for visitors</p>

                    </div>
                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="feature-box wow fadeIn" data-wow-delay="1.5s"
                         style="visibility: visible; animation-delay: 1.5s; animation-name: fadeIn;">

                        <i class="fab fa-rocketchat"></i>

                        <h4>Support 24/7</h4>

                        <p>Our team is full of photographers, so we know getting started can be scary. We’re here to
                            help
                            get your online gallery up and running in no time.</p>

                    </div>
                </div>
                <!--end col-md-4 -->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </section>
    <!--end services section -->


    <!--begin pricing section -->
    <section class="section-grey" id="pricing">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-12 -->
                <div class="col-md-12 text-center padding-bottom-40">

                    <h2 class="section-title">Pricing for every business, at any stage</h2>

                    <p class="section-subtitle">All pricing packages are backed up by a 30-day money back guarantee.</p>

                </div>
                <!--end col-md-12 -->

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="price-box">

                        <ul class="pricing-list">

                            <li class="price-title">BASIC</li>

                            <li class="price-value"><sup>KES</sup>0</li>

                            <li class="price-subtitle">Per Month</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>1 Gb of Storage</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>JPG uploads only</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>PichaClick Branding</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>15% Commission on all sales</li>

                            <li class="price-tag-line"><a href="{{ route('register') }}">Get Started</a></li>

                        </ul>

                    </div>

                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="price-box">

                        <div class="ribbon blue"><span>Popular</span></div>

                        <ul class="pricing-list">

                            <li class="price-title">STANDARD</li>

                            <li class="price-value"><sup>KES</sup>600</li>

                            <li class="price-subtitle">Per Month</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>15 Gb of Storage</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>0% Commission on sales</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>Custom Branding</li>
                            <li class="price-text"> &nbsp;</li>

                            <li class="price-tag"><a href="{{ route('register') }}">FREE 15-DAY TRIAL</a></li>

                        </ul>

                    </div>

                </div>
                <!--end col-md-4 -->

                <!--begin col-md-4-->
                <div class="col-md-4">

                    <div class="price-box">

                        <ul class="pricing-list">

                            <li class="price-title white-text">PRO</li>

                            <li class="price-value white-text"><sup>KES</sup>900</li>

                            <li class="price-subtitle white-text">Per Month</li>

                            <li class="price-text white-text"><i class="fas fa-check blue"></i><strong>200 Gb of
                                    Storage</strong></li>

                            <li class="price-text"><i class="fas fa-check blue"></i>0% Commission on sales</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>Custom Branding</li>

                            <li class="price-text"><i class="fas fa-check blue"></i>24/7 Priority Support</li>

                            <li class="price-tag-line"><a href="{{ route('register') }}">FREE 15-DAY TRIAL</a></li>

                        </ul>

                    </div>

                </div>
                <!--end col-md-4 -->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </section>
    <!--end pricing section -->


    <section class="section-blue small-paddings">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-12 -->
                <div class="col-md-12 text-center padding-top-50 padding-bottom-20">

                    <h3 class="white-text">Seen enough? We are currently in the closed beta.<br>
                        Please enter your email to join the waiting list.</h3>

                </div>
                <!--end col-md-12 -->

                <!--begin col-md-12-->
                <div class="col-md-12 padding-bottom-40">

                    <!--begin newsletter_form_wrapper -->
                    <div class="newsletter_form_wrapper wow bounceIn" data-wow-delay="0.25s"
                         style="visibility: visible; animation-delay: 0.25s; animation-name: bounceIn;">

                        <!--begin newsletter_form_box -->
                        <div class="newsletter_form_box">

                            <!--begin success_box -->
                            <p class="newsletter_success_box" v-if="successful">We received your message and you'll
                                hear
                                from us soon. Thank You! </p>
                            <!--end success_box -->

                            <!--begin newsletter-form -->
                            <form id="newsletter-form" class="newsletter-form"
                                  action="" method="post">

                                <input id="email_newsletter" type="email" required
                                       v-model="email"
                                       placeholder="Enter Your Email Address">

                                <button type="submit" id="submit-button-newsletter" disabled
                                        v-on:click.prevent="submitNewsletter" class="text-primary">
                                    Get Early Access <i :class="{'fa fa-spin fa-spinner': submitting}"></i>
                                </button>
                                {{--<i class="fa fa-spin fa-spinner"></i>--}}
                            </form>
                            <!--end newsletter-form -->

                        </div>
                        <!--end newsletter_form_box -->

                    </div>
                    <!--end newsletter_form_wrapper -->

                </div>
                <!--end col-md-12 -->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </section>
    <!--end newsletter section -->


    <!--begin contact -->
    <section class="section-white section-top-border" id="contact">

        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row">

                <!--begin col-md-6 -->
                <div class="col-md-6">

                    <h4>Get in touch <i class="fa fa-spin text-danger"></i>
                    </h4>

                    <!--begin success message -->
                    <p class="contact_success_box" style="display:none;">We received your message and you'll hear from
                        us
                        soon. Thank You!</p>
                    <!--end success message -->

                    <!--begin contact form -->
                    <form id="contact-form" class="contact" method="post">

                        <input class="contact-input white-input" required="" name="contact_names"
                               placeholder="Full Name*" v-model="contactname"
                               type="text">

                        <input class="contact-input white-input" required="" name="contact_email"
                               placeholder="Email Adress*" type="email" v-model="contactemail">

                        <input class="contact-input white-input" required="" name="contact_phone"
                               placeholder="Phone Number*" type="text" v-model="contactphone">

                        <textarea class="contact-commnent white-input" rows="2" cols="20" name="contact_message"
                                  v-model="contactmessage"
                                  placeholder="Your Message..."></textarea>

                        <input value="Send Message" id="submit-button" class="contact-submit"
                               v-on:click.prevent="submitticket" type="submit">

                    </form>
                    <!--end contact form -->

                </div>
                <!--end col-md-6 -->

                <!--begin col-md-6 -->
                <div class="col-md-6">

                    <h4>How to find us</h4>
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe width="600" height="500" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=compwhiz&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            {{--<a href="https://www.pureblack.de/webdesign-duesseldorf/">internetagentur duesseldorf</a>--}}
                        </div>
                        <style>.mapouter {
                                text-align: right;
                                height: 500px;
                                width: 600px;
                            }

                            .gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                                height: 500px;
                                width: 600px;
                            }</style>

                    </div>
                    {{--<iframe class="contact-maps"--}}
                    {{--src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.9050207912896!2d-0.14675028449633118!3d51.514958479636384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ad554c335c1%3A0xda2164b934c67c1a!2sOxford+St%2C+London%2C+UK!5e0!3m2!1sen!2sro!4v1485889312335"--}}
                    {{--width="600" height="270" style="border:0" allowfullscreen></iframe>--}}

                    <h5>Head Office</h5>

                    <p class="contact-info"><i class="fas fa-map-marker-alt"></i> 132 &mdash;90138, Makindu</p>

                    <p class="contact-info"><i class="fas fa-envelope"></i> <a href="mailto:kenmsh@gmail
                    .com">sales@pichaclick.co.ke</a>
                    </p>

                    <p class="contact-info"><i class="fas fa-phone"></i> +254 713 642 175</p>

                </div>
                <!--end col-md-6 -->

            </div>
            <!--end row-->

        </div>
        <!--end container-->

    </section>
    <!--end contact-->

    <!--begin footer -->
    <div class="footer">

        <!--begin container -->
        <div class="container">

            <!--begin row -->
            <div class="row">

                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">

                    <p>Copyright © {{ date('Y') }} PichaClick. A product of Compwhiz Tech
                    </p>

                    <!--begin footer_social -->
                    <ul class="footer_social">

                        <li>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fab fa-pinterest"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fab fa-skype"></i>
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fab fa-dribble"></i>
                            </a>
                        </li>

                    </ul>
                    <!--end footer_social -->

                </div>
                <!--end col-md-6 -->

            </div>
            <!--end row -->

        </div>
        <!--end container -->

    </div>
    <!--end footer -->

</div>

<!-- Load JS here for greater good =============================-->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('pub/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('pub/js/bootstrap.min.js')}}"></script>
<script src="{{asset('pub/js/jquery.scrollTo-min.js')}}"></script>
<script src="{{asset('pub/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('pub/js/jquery.nav.js')}}"></script>
<script src="{{asset('pub/js/wow.js')}}"></script>
<script src="{{asset('pub/js/plugins.js')}}"></script>
<script src="{{asset('pub/js/custom.js')}}"></script>


</body>
</html>
{{--<div class="RY3tic"--}}
{{--     style="background-image: url(&quot;&quot;), url(&quot;https:&quot;); opacity: 1; ;"--}}
{{--     data-latest-bg="https://lh3.googleusercontent.com/lL-6m4VO4hjfkGOplBdZjuPHP2OOdrYYbd3leAG6Z3ZGWIFO7-tbROkgie-W0FpahgC5OeUUV1R3_Z8KpBru9ThsXI0RzKwznZ9PcF88pb9KlXX6i3jX5nDtDqyWozTV6TTFoS3ayLKFoCrD_99hVDC_zi-gh9eACqLPW_gJF5sIpb_uMtsXkOhzyb1fhral3jPzS75KYurbNKuYnphsWxNBg1SdBrJBjZ7cmqP7W90ru_f1ve0-5rwLDmMtM90M-9jDq5vHQWMt_s6MnxqZZGXAfX5NIoG8bTtbOWQ1m_1AetMSvEyoMgPTsnl5WW9XZMr94qKViJM-QC5lmArNpIjUDM-8RIllgMFerqejuf_h0Mad-aP4b2Nkyl0aqeyf66L7kyDFzW5YlbrZZHFrmXBHD0wxF9fL-Gs0Ih3jsBsdHWvSg0tVvdxcT9VXNmJO5ZAyYWqGUqFQwWBEsIURTNF885pfLdlvCKMYqBLB6lF8SwmngBIcTR3EsiUknZSUa4S794qBjNyyYrmotMUKVgWgxVgxLbYReTzRxXXLEaIArNjY6bNPFltzjv_82hRgRO4z3cd5r-N2f9bWVKqph56RuPqEuSMI63bKkyGbpHu2btj_QSNaRZawvOGSovwKLLlOVuLpyLVFlBNVn0nkbyz4xOVnGTIO=w114-h227-no">--}}
{{--    <div class="eGiHwc" aria-hidden="true"></div>--}}
{{--    <div class="KYCEmd" aria-hidden="true"></div>--}}
{{--</div>--}}
