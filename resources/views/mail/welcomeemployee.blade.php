@component('mail::message')
##Dear Sir/Madam,

Please send me a copy of your catalogue and price list together with copies of any descriptive
leaflets.

Yours truly,

Kennedy Mutisya,<br>
Operations Manager,<br>
Compwhiz Technology Solutions
@endcomponent
